import { Component } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { NavController, ToastController } from 'ionic-angular';
import { user } from "../../app/models/user";
import { ReflistPage } from '../reflist/reflist';
import { RegisterPage } from '../register/register';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[AngularFireAuth]
})
export class HomePage {

  objUserApp = {} as user;
  constructor(public navCtrl: NavController, public auth: AngularFireAuth, public toast: ToastController) {}

  goToRegister(){
    this.navCtrl.push(RegisterPage);
  }

  login(){

    this.auth.auth.signInWithEmailAndPassword(this.objUserApp.email, this.objUserApp.password).then((data)=>{
      if(!data.isNewUser){
        this.navCtrl.setRoot(ReflistPage);
      }
    }).catch((e)=>{
      this.toast.create({
        message:e.message,
        duration: 2000
      }).present();
    });    
  }

}
