import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { user } from '../../app/models/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { ReflistPage } from '../reflist/reflist';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
  providers:[AngularFireAuth]
})
export class RegisterPage {
  objUserApp = {} as user;
  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AngularFireAuth) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  async register(){
    try{
      const res = this.auth.auth.createUserWithEmailAndPassword(this.objUserApp.email, this.objUserApp.password);
      if(res){
        this.navCtrl.setRoot(ReflistPage);
      }
    }catch (e){
      console.error(e);
    }
  }

}
