import { Component } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { ref } from '../../app/models/ref';
import { AddrefPage } from '../addref/addref';
import { ShowrefPage } from '../showref/showref';

/**
 * Generated class for the ReflistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reflist',
  templateUrl: 'reflist.html',
  providers:[AngularFireDatabase]
})
export class ReflistPage {
  items: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public FBData: AngularFireDatabase) {
    this.FBData.list('/dataRefs/').valueChanges().subscribe((dataReferences)=>{
      this.items = dataReferences;
    });
  }
  
  itemSelected(item: ref){
    this.navCtrl.push(ShowrefPage,{reference:item});
  }

  addItem(){
    this.navCtrl.push(AddrefPage);
  }

}
