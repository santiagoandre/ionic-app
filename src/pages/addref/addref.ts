import { Component } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { ToastController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ref } from '../../app/models/ref';
import { ReflistPage } from '../reflist/reflist';

/**
 * Generated class for the AddrefPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addref',
  templateUrl: 'addref.html',
  providers:[AngularFireDatabase]
})
export class AddrefPage {
  reference = {} as ref;
  constructor(public navCtrl: NavController, public navParams: NavParams, public BD: AngularFireDatabase, public toast:ToastController) {
    this.reference = navParams.data.reference || {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddrefPage');
  }

  saveref(){
    if(!this.reference.idreferencia){
      this.reference.idreferencia = Date.now();
    }
    try{
      this.BD.database.ref('/dataRefs/' + this.reference.idreferencia).set(this.reference);
      this.toast.create({
        message:"Add reference success",
        duration:2000
      }).present();
      this.navCtrl.setRoot(ReflistPage);
    }
    catch (e){
      this.toast.create({
        message:"Add failed",
        duration: 2000
      }).present();
    }
  }

}
